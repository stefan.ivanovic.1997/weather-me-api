import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from "./auth/auth.module";
import { WeatherModule } from './weather/weather.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    AuthModule,
    WeatherModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

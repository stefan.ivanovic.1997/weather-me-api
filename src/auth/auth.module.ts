import { Module } from "@nestjs/common";
import { AuthController } from "./controllers/auth.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserRepository } from "./repository/user.repository";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { jwtConstants } from "../core/constants";
import { UserService } from "./service/user.service";
import { JwtStrategy } from "./strategy/jwt.strategy";
import { UserController } from "./controllers/user.controller";
import { FavouriteRepository } from "../weather/repository/favourite.repository";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      FavouriteRepository
    ]),
    PassportModule.register({ defaultStrategy: "jwt" }),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {}
    })
  ],
  providers: [
    JwtStrategy,
    UserService
  ],
  exports: [
    JwtStrategy,
    PassportModule
  ],
  controllers: [
    AuthController,
    UserController]
})
export class AuthModule {

}

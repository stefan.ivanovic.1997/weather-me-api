import { Body, Controller, Post, ValidationPipe } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { LoginDto } from "../dto/request/login.dto";
import { UserService } from "../service/user.service";
import { RegisterDto } from "../dto/request/register.dto";
import { UserDataDto } from "../dto/response/user-data.dto";

@ApiTags("Auth")
@Controller("auth")
export class AuthController {

  constructor(private userService: UserService) {
  }

  @Post("login")
  async login(@Body(ValidationPipe) loginDto: LoginDto): Promise<UserDataDto> {
    return this.userService.login(loginDto);
  }

  @Post("register")
  async register(@Body(ValidationPipe) registerDto: RegisterDto): Promise<UserDataDto> {
    return this.userService.createUser(registerDto);
  }

}


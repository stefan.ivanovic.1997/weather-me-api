import { Body, Controller, Get, Param, ParseIntPipe, Post, UseGuards, ValidationPipe } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { UserService } from "../service/user.service";
import { AuthGuard } from "@nestjs/passport";
import { GetUser } from "../../core/decorators/decorators";
import { User } from "../entity/user.entity";
import { UserDataDto } from "../dto/response/user-data.dto";
import { UpdateBodyDto } from "../dto/request/updateBody.dto";

@ApiTags("User")
@Controller("users")

export class UserController {

  constructor(private userService: UserService) {
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  @Post("fahrenheit")
  async update(@GetUser() user: User, @Body(ValidationPipe) updateBody: UpdateBodyDto): Promise<UserDataDto> {
    return this.userService.updateFahrenheit(user, updateBody);
  }

  @Get("/:id")
  async getUser(@Param("id", ParseIntPipe) userId: number): Promise<UserDataDto> {
    return this.userService.getUser(userId);
  }

}


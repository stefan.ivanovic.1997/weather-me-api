import { IsBoolean } from "class-validator";

export class UpdateBodyDto {

  @IsBoolean()
  fahrenheit: boolean;

}

import { FavouriteEntity } from "../../../weather/entity/favourite.entity";

export class UserDataDto {
  id: number;
  name: string;
  username: string;
  lastName: string;
  accessToken?: string;
  fahrenheit: boolean;
  favourites?: FavouriteEntity[];
}

import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { FavouriteEntity } from "../../weather/entity/favourite.entity";

@Entity()
export class User extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, unique: true })
  username: string;

  @Column({ nullable: false })
  password: string;

  @Column({ nullable: false })
  salt: string;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  lastName: string;

  @Column({ nullable: false, default: true })
  fahrenheit: boolean;

  @OneToMany(
    type => FavouriteEntity,
    favourite => favourite.user,
  )
  favourites: FavouriteEntity[];
}

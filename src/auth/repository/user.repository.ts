import { EntityRepository, Repository } from "typeorm";
import { User } from "../entity/user.entity";
import { UserDataDto } from "../dto/response/user-data.dto";
import { RegisterDto } from "../dto/request/register.dto";
import * as bcrypt from "bcryptjs";
import { ConflictException } from "@nestjs/common";
import { LoginDto } from "../dto/request/login.dto";

@EntityRepository(User)
export class UserRepository extends Repository<User> {

  async createUser(registerDto: RegisterDto): Promise<UserDataDto> {
    const { name, password, lastName, username } = registerDto;

    const user = new User();
    user.salt = await bcrypt.genSalt();
    user.password = await UserRepository.hashPassword(password, user.salt);
    user.username = username;
    user.name = name;
    user.lastName = lastName;

    try {
      const res = await user.save();
      return {
        id: res.id,
        name: res.name,
        lastName: res.lastName,
        username: res.username,
        fahrenheit: res.fahrenheit
      };
    } catch (error) {
      if (error.code === "23505") { // duplicate username
        throw new ConflictException("User with this username already exist!");
      }
      throw error;
    }
  }

  async getUser(id: number) {
    const user = this.createQueryBuilder('user')
      .leftJoinAndSelect('user.favourites', 'favourites').where({id}).getOne();
    return user;
  }

  async validateCredentials(loginDto: LoginDto) {
    const { username, password } = loginDto;
    const user = await this.findOne({ username: username });
    if (user && await UserRepository.validatePassword(password, user)) {
      return user;
    }
    return null;
  }

  private static async validatePassword(password: string, user: User): Promise<boolean> {
    const hash = await bcrypt.hash(password, user.salt);
    return hash === user.password;
  }

  private static async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

}

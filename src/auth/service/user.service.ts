import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common";
import { RegisterDto } from "../dto/request/register.dto";
import { JwtService } from "@nestjs/jwt";
import { UserRepository } from "../repository/user.repository";
import { InjectRepository } from "@nestjs/typeorm";
import { UserDataDto } from "../dto/response/user-data.dto";
import { LoginDto } from "../dto/request/login.dto";
import { User } from "../entity/user.entity";
import { JwtPayload } from "../strategy/jwt.strategy";
import { UpdateBodyDto } from "../dto/request/updateBody.dto";

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    private readonly jwtService: JwtService
  ) {
  }

  async getUser(id: number): Promise<UserDataDto> {
    return this.userRepository.getUser(id);
  }

  async createUser(registerDto: RegisterDto): Promise<UserDataDto> {
    try {
      return this.userRepository.createUser(registerDto);
    } catch (e) {
      if (e.code && e.code.startsWith("auth/")) {
        throw new BadRequestException(e.message);
      }
      throw e;
    }
  }

  async login(loginDto: LoginDto): Promise<UserDataDto> {
    let user: User = await this.userRepository.validateCredentials(loginDto);
    if (!user) {
      throw new UnauthorizedException("Wrong username or password!");
    }
    user = await this.userRepository.getUser(user.id);

    return {
      accessToken: await this.createAccessToken(user),
      name: user.name,
      username: user.username,
      lastName: user.lastName,
      id: user.id,
      fahrenheit: user.fahrenheit,
      favourites: user.favourites
    };
  }

  async updateFahrenheit(user: User, updateBody: UpdateBodyDto) {
    const dbUser = await this.userRepository.findOne(user.id);

    dbUser.fahrenheit = updateBody.fahrenheit;
    await dbUser.save();

    return {
      name: user.name,
      username: user.username,
      lastName: user.lastName,
      id: user.id,
      fahrenheit: user.fahrenheit,
      favourites: user.favourites
    };
  }

  private async createAccessToken(user: User): Promise<string> {
    const { username, id, lastName, name } = user;
    const payload: JwtPayload = { id, username, lastName, name };
    return this.jwtService.sign(payload);
  }

}

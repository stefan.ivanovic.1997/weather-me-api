export const jwtConstants = {
  secret: "weatherMe"
};
export const ACCU_KEY = "fZG69XM2t8xm51ee6782ZJcVkVCVG4mV";
export const ACCU_URL = "http://dataservice.accuweather.com";

export const LOCATION_SERVICE = 'locations';
export const WEATHER_SERVICE = 'forecasts';

export const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const SHORT_DATE_FORMAT = 'DD.MMMM';

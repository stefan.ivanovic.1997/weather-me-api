import { createParamDecorator, ExecutionContext } from "@nestjs/common";

export const GetUser = createParamDecorator((data, ctx: ExecutionContext): any => {
  return ctx.switchToHttp().getRequest().user;
});



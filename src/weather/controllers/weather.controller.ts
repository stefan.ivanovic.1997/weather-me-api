import {
  BadRequestException,
  Body,
  Controller, Delete,
  Get,
  Param, ParseIntPipe, Post,
  Query,
  UseGuards, ValidationPipe
} from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { WeatherService } from "../service/weather.service";
import { AuthGuard } from "@nestjs/passport";
import { Observable, of } from "rxjs";
import { GetUser } from "../../core/decorators/decorators";
import { User } from "../../auth/entity/user.entity";
import { FavouritesDto } from "../dto/response/favourites.dto";
import { FavouriteEntity } from "../entity/favourite.entity";
import { GeolocationDto } from "../dto/request/geolocation.dto";

@ApiTags("Weather")
@Controller("weather")
@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
export class WeatherController {

  constructor(private weatherService: WeatherService) {
  }

  @Get("favourites")
  async getFavourites(@GetUser() user: User): Promise<FavouritesDto[]> {
    return this.weatherService.getFavourites(user);
  }

  @Post("favourites")
  async addFavourites(@GetUser() user: User, @Body(ValidationPipe) favouritesDto: FavouritesDto): Promise<FavouriteEntity> {
    return this.weatherService.addFavourites(favouritesDto);
  }

  @Delete("favourites/:id")
  async deleteFavourites(@GetUser() user: User, @Param("id", ParseIntPipe) id: number): Promise<void> {
    return this.weatherService.deleteFavourites(id);
  }

  @Get("geolocation")
  getGeolocation(@GetUser() user: User, @Query(ValidationPipe) query: GeolocationDto): Observable<any> {
    return this.weatherService.getGeolocation(query);
  }

  @Get("locations")
  getLocations(@Query() query): Observable<any> {
    if (!!query.q) {
      return this.weatherService.getLocations(query.q);
    }
    return of([]);
  }

  @Get("/:locationKey")
  getWeather(@Param("locationKey", ParseIntPipe) locationKey: number, @Query() query): Observable<any> {
    return this.weatherService.getWeather(locationKey, query.fahrenheit === "true");
  }

}


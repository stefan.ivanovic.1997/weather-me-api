import { IsDecimal, IsNotEmpty } from "class-validator";

export class GeolocationDto {

  @IsNotEmpty()
  @IsDecimal()
  lat: number;

  @IsNotEmpty()
  @IsDecimal()
  long: number;
}

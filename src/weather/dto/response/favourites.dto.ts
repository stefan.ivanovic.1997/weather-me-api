export class FavouritesDto {
  id?: number;
  userId?: number;
  locationKey?: number;
  country?: string;
  city?: string;
  forecast?: any;
}

export class Forecast {
  day?: string;
  date?: string;
  temperature?: { minimum?: number, maximum?: number, unit?: string };
  description?: string;
}

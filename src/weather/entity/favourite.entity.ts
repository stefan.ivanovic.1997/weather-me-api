import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../../auth/entity/user.entity";

@Entity()
export class FavouriteEntity extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  locationKey: number;

  @Column({ nullable: false })
  userId: number;

  @Column({ nullable: false })
  city: string;

  @Column({ nullable: false })
  country: string;

  @ManyToOne(
    type => User,
    user => user.favourites
  )
  user: User;
}

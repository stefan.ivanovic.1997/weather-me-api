import { EntityRepository, Repository } from "typeorm";
import { FavouriteEntity } from "../entity/favourite.entity";

@EntityRepository(FavouriteEntity)
export class FavouriteRepository extends Repository<FavouriteEntity> {

  async getFavouritesForUser(id: number) {
    return await this.find({ where: { userId: id } });
  }

}

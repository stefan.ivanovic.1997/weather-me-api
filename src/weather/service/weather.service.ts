import { Injectable } from "@nestjs/common";
import { HttpService } from "@nestjs/axios";
import {
  ACCU_KEY,
  ACCU_URL,
  DATE_FORMAT,
  LOCATION_SERVICE,
  SHORT_DATE_FORMAT,
  WEATHER_SERVICE
} from "../../core/constants";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { FavouritesDto, Forecast } from "../dto/response/favourites.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { FavouriteRepository } from "../repository/favourite.repository";
import { FavouriteEntity } from "../entity/favourite.entity";
import * as moment from "moment";
import { GeolocationDto } from "../dto/request/geolocation.dto";
import { User } from "../../auth/entity/user.entity";

@Injectable()
export class WeatherService {

  constructor(
    private httpClient: HttpService,
    @InjectRepository(FavouriteRepository)
    private favouriteRepository: FavouriteRepository
  ) {
  }

  getLocations(q: string): Observable<any> {
    const url = WeatherService.generateUrl(LOCATION_SERVICE, `cities/autocomplete?q=${q}`);
    return this.httpClient.get(url).pipe(map(res => res.data));
  }

  getWeather(locationKey: number, fahrenheit: boolean): any {
    const url = WeatherService.generateUrl(WEATHER_SERVICE, `daily/5day/${locationKey}?metric=${!fahrenheit}`);
    return this.httpClient.get(url).pipe(map(res => this.mapWeatherData(res.data)));
  }

  getWeatherDaily(locationKey: number, fahrenheit: boolean): any {
    const url = WeatherService.generateUrl(WEATHER_SERVICE, `daily/1day/${locationKey}?metric=${!fahrenheit}`);
    return this.httpClient.get(url).pipe(map(res => this.mapWeatherData(res.data)));
  }

  mapWeatherData(data: any): any[] {
    const newData = [];
    if (data) {
      if (data.DailyForecasts) {
        data.DailyForecasts.forEach(daily => {
          const obj: Forecast = {};
          obj.day = moment(daily.Date, DATE_FORMAT).format("dddd");
          obj.date = moment(daily.Date, DATE_FORMAT).format(SHORT_DATE_FORMAT);
          obj.temperature = {
            minimum: daily.Temperature.Minimum.Value,
            maximum: daily.Temperature.Maximum.Value,
            unit: daily.Temperature.Minimum.Unit
          };
          obj.description = daily.Day.IconPhrase;
          newData.push(obj);
        });
      }
    }
    return newData;
  }

  async getFavourites(user: User): Promise<FavouritesDto[]> {
    const data: FavouritesDto[] = [];
    const favourites = await this.favouriteRepository.getFavouritesForUser(user.id);
    for(const elem of favourites){
      const weather = await this.getWeatherDaily(elem.locationKey, !user.fahrenheit).toPromise();
      const obj = {
        ...elem,
        forecast: !!weather && weather.length > 0 ? weather[0] : null
      }
      data.push(obj);
    }

    return data;
  }

  async addFavourites(favourite: FavouritesDto): Promise<FavouriteEntity> {
    const favouriteEntity = new FavouriteEntity();
    favouriteEntity.userId = favourite.userId;
    favouriteEntity.locationKey = favourite.locationKey;
    favouriteEntity.city = favourite.city;
    favouriteEntity.country = favourite.country;
    return await favouriteEntity.save();
  }

  async deleteFavourites(id: number): Promise<void> {
    await this.favouriteRepository.delete({ id });
  }

  private static generateUrl(service: string, endpoint: string): string {
    return `${ACCU_URL}/${service}/v1/${endpoint}&apikey=${ACCU_KEY}`;
  }

  getGeolocation(data: GeolocationDto) {
    const q = data.lat + "," + data.long;
    const url = WeatherService.generateUrl(LOCATION_SERVICE, `cities/geoposition/search?q=${q}`);
    return this.httpClient.get(url).pipe(map(res => res.data));
  }


}

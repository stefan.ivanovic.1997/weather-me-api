import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { HttpModule } from "@nestjs/axios";
import { WeatherService } from "./service/weather.service";
import { WeatherController } from "./controllers/weather.controller";
import { FavouriteRepository } from "./repository/favourite.repository";
import { UserRepository } from "../auth/repository/user.repository";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FavouriteRepository,
      UserRepository
    ]),
    HttpModule
  ],
  providers: [
    WeatherService
  ],
  controllers: [WeatherController]
})
export class WeatherModule {

}
